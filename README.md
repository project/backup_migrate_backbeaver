# Backup and Migrate: BackBeaver

This module allows BackBeaver to be used as a destination
for Backup and Migrate files.

## Requirements ##

Modules required:
- [Backup and Migrate](https://www.drupal.org/project/backup_migrate)


## Installation

It is suggested that you install using Composer.

```bash
cd /path/to/drupal/root
composer require drupal/backup_migrate_backbeaver
drush en backup_migrate_backbeaver
```

## Configuration ##

1. Go to [BackBeaver.com](https://backbeaver.com) then login or register, and then add your site. When creating a Site choose plan that suits you most: Free, Basic or Pro. After you registered a Site you will get Site API key, store it securely.
2. Then in your Drupal site go to Configuration -> Backup and Migrate -> Settings -> Destinations (/admin/config/development/backup_migrate/settings/destination) and click on Add Backup Destination in order to register BackBeaver as a backup destination.
3. On the next screen choose appropriate label (ex. Mysite on BackBeaver) for your Backup and under Type select BackBeaver
4. On the next screen enter your Site API key that you got from BackBeaver dashboard when you registered a site.
5. And thats it now you can do database, private, public or entire site backups using Backup and Migrate module either manually (/admin/config/development/backup_migrate) or set an automated Schedule (/admin/config/development/backup_migrate/schedule). Make sure that under Backup destination you select the destination you created in previous step (ex. Mysite on BackBeaver).

## Maintainers ##

Tamer Zoubi (tamerzg)
https://www.drupal.org/u/tamerzg
