<?php

namespace Drupal\backup_migrate_backbeaver\Controller;

use Drupal\backup_migrate\Controller\BackupController;
use Drupal\backup_migrate\Entity\Destination;
use Drupal\backup_migrate_backbeaver\Destination\BackBeaverDestination;

/**
 * BackBeaver Backup & Migrate Controller.
 *
 * @package Drupal\backup_migrate_backbeaver\Controller
 */
class BackBeaverBackupController extends BackupController {

  /**
   * Backup & migrate destination object.
   *
   * @var \Drupal\backup_migrate\Core\Destination\DestinationInterface
   */
  public $destination;

  /**
   * Download a backup via the browser.
   *
   * @param \Drupal\backup_migrate\Entity\Destination $backup_migrate_destination
   *   Backup & migrate destination object.
   * @param string $backup_id
   *   Backup & migrate backup file id.
   *
   * @return \Symfony\Component\HttpFoundation\Response|void
   *   Return http response to download backup file.
   *
   * @throws \Drupal\backup_migrate\Core\Exception\BackupMigrateException
   */
  public function download(Destination $backup_migrate_destination, $backup_id) {
    $destination = $backup_migrate_destination->getObject();

    if ($destination instanceof BackBeaverDestination) {
      return $destination->downloadFile($backup_id);
    }
    else {
      parent::download($backup_migrate_destination, $backup_id);
    }
  }
}
