<?php

namespace Drupal\backup_migrate_backbeaver\Plugin\BackupMigrateDestination;

use Drupal\backup_migrate\Drupal\EntityPlugins\DestinationPluginBase;

/**
 * Defines a file directory destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "backbeaver",
 *   title = @Translation("BackBeaver"),
 *   description = @Translation("Backup to BackBeaver."),
 *   wrapped_class = "\Drupal\backup_migrate_backbeaver\Destination\BackBeaverDestination"
 * )
 *
 * Note: It is unclear why the plugin is constructed with wrapped_class. To keep
 * things consistent, the deprecated Psr4ClassLoader is called in
 * backup_migrate.module.
 *
 * @see \Drupal\backup_migrate\Drupal\EntityPlugins\WrapperPluginBase::getObject()
 */
class BackBeaverDestinationPlugin extends DestinationPluginBase {
}
