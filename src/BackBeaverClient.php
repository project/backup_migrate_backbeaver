<?php
//Class that connects to the BackBeaver JSON API.

namespace Drupal\backup_migrate_backbeaver;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use ReflectionException;

class BackBeaverClient{
  /**
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * @var string
   */

  protected $apiKey;

  /**
   * BackBeaverClient constructor.
   *
   * @param $http_client_factory ClientFactory
   */
  public function __construct($http_client_factory) {
    $this->client = $http_client_factory->fromOptions([
      'base_uri' => $this->getServers()[0]
    ]);
  }

  /**
   * Returns list of BackBeaver API servers
   */
  private function getServers(){
    $servers=[
      'https://api1.backbeaver.com',
      'https://api2.backbeaver.com',
      'https://api3.backbeaver.com',
      'https://api4.backbeaver.com',
      'https://api5.backbeaver.com',
    ];
    return $servers;
  }

  /**
   * Set API key
   */
  public function setApiKey($apiKey) {
    //@todo get API key in constructor from entity storage?
    $this->apiKey = $apiKey;
    return $this;
  }

  /**
   * Get Files
   * @return array
   */
  public function getFiles($page=1, $limit=100, $sortBy='created', $orderBy='DESC') {
    try{
      $response = $this->client->get('/api/v1/files',[
          'headers' =>['X-AUTH-TOKEN' => $this->apiKey, ],
          'query' => [
            'page' => $page,
            'limit' => $limit,
            'sortBy' => $sortBy,
            'orderBy' => $orderBy,
          ],
        ]);
      if($response->getBody()) {
        $body = Json::decode($response->getBody());
        if($body) {
          return $body['files'];
        }
      }
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      \Drupal::logger('backup_migrate_backbeaver')->error($response->getBody());

      $response_body = Json::decode($response->getBody());
      $error['error'] = $response_body['message'];
      return $error;
    }
    catch (GuzzleException $e){
      //@todo
      \Drupal::logger('backup_migrate_backbeaver')->error($e->getMessage());
      $error['error'] = $e->getMessage();
      return $error;
    }
    return NULL;
  }

  /**
   * Delete File
   *
   * @param string $filename
   *
   * @return boolean
   */
  public function deleteFile($filename) {
    try{
      $response = $this->client->delete('/api/v1/files/'.$filename, [ 'headers' =>['X-AUTH-TOKEN' => $this->apiKey]]);

      //@todo: check response and return TRUE if file was deleted
      if($response->getStatusCode() == 200) {
        return TRUE;
      }
      //Something went wrong
      \Drupal::logger('backup_migrate_backbeaver')->error('Error deleting file. Status code: %code', ['%code' =>$response->getStatusCode()]);
      return FALSE;
    }
    catch (ClientException $e) {
      \Drupal::logger('backup_migrate_backbeaver')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Download File
   *
   * @param string $filename
   *
   * @return $file
   */
  public function downloadFile($filename) {
    try{
      $response = $this->client->get('/api/v1/files/download/'.$filename, [
        'headers' =>['X-AUTH-TOKEN' => $this->apiKey],
        'stream' => TRUE,
        ]);
      return $response;
    }
    catch (ClientException $e) {
      //@todo: error handling
      $response = $e->getResponse();
      $responseBodyAsString = $response->getBody()->getContents();
    }
  }

  /**
   * Upload File
   *
   * @param file $file
   *
   *
   */
  public function uploadFile($fileloc, $filename) {
    set_time_limit(0); // do as long as it takes
    //@todo: replace with Commercial API
    $baseUrl = $this->getServers()[0];
    $key = uniqid(); //STORE THIS ID FOR LATER POSSIBLE RESUME OF REQUEST.
    $options["headers"] = [
      "X-AUTH-TOKEN" => $this->apiKey
    ];
    try {
      $client = new \TusPhp\Tus\Client($baseUrl, $options);
      $client->setApiPath("/api/v1/files");
      $client->setKey($key)->file($fileloc, $filename);
      $total = $client->getFileSize();
      $limit = 50000000; // This will be set as memory limit 50M on the server. It will deny request if is more than this.
      $total_chunks = ceil($total / $limit);
      if($total < $limit){
        $limit = $total;
      }
      $i=0;
      while($i < $total_chunks) {
        $client->upload($limit);
        $i++;
      }
      //$client->delete();
    } catch (ReflectionException |  GuzzleException  $e) {
      //@todo do something with exception
      \Drupal::logger('backup_migrate_backbeaver')->error($e->getMessage());
    }
  }
}
