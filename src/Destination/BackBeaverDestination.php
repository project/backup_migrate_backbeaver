<?php

namespace Drupal\backup_migrate_backbeaver\Destination;


use Drupal\backup_migrate\Core\Config\ConfigInterface;
use Drupal\backup_migrate\Core\Config\ConfigurableInterface;
use Drupal\backup_migrate\Core\Destination\RemoteDestinationInterface;
use Drupal\backup_migrate\Core\Destination\ListableDestinationInterface;
use Drupal\backup_migrate\Core\File\BackupFile;
use Drupal\backup_migrate\Core\Destination\ReadableDestinationInterface;
use Drupal\backup_migrate\Core\File\BackupFileInterface;
use Drupal\backup_migrate\Core\File\BackupFileReadableInterface;
use Drupal\backup_migrate\Core\Destination\DestinationBase;
use Drupal\backup_migrate\Core\File\ReadableStreamBackupFile;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;


/**
 * Class BackBeaverDestination.
 *
 * @package Drupal\backup_migrate_backbeaver\Drupal\Destination
 */
class BackBeaverDestination extends DestinationBase implements RemoteDestinationInterface, ListableDestinationInterface, ReadableDestinationInterface, ConfigurableInterface {

  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * File repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository = NULL;

  /**
   * Filesystem service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem = NULL;

  /**
   * @var \Drupal\backup_migrate_backbeaver\BackBeaverClient
   */
  protected $backBeaverClient;


  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigInterface $config) {
    parent::__construct($config);

    /** @codingStandardsIgnoreStart */
    $this->fileRepository = \Drupal::service('file.repository');
    $this->fileSystem = \Drupal::service('file_system');
    $this->backBeaverClient = \Drupal::service('backbeaver_client');
    /** @codingStandardsIgnoreEnd */
  }

  /**
   * {@inheritdoc}
   */
  public function checkWritable(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteTheFile($id) {
    $api_key = $this->confGet('backbeaver_site_api_key');
    $this->backBeaverClient->setApiKey($api_key);
    $success = $this->backBeaverClient->deleteFile($id);
    if($success){
      \Drupal::messenger()->addMessage(t('File deleted successfully.'), 'status');
    }
    else{
      \Drupal::messenger()->addMessage(t('Error deleting file, please check Recent log messages for more details.'), 'error');
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\backup_migrate\Core\Exception\BackupMigrateException
   */
  protected function saveTheFile(BackupFileReadableInterface $file) {
    $api_key = $this->confGet('backbeaver_site_api_key');
    $this->backBeaverClient->setApiKey($api_key);

    //@todo: Must close file because meta files appear to be left open by backup_migrate
    //$file->close();

    $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($file->realpath());
    $file_path = $stream_wrapper_manager->realpath();
    $this->backBeaverClient->uploadFile($file_path, $file->getFullName());
  }

  /**
   * {@inheritdoc}
   */
  protected function saveTheFileMetadata(BackupFileInterface $file) {
    // Nothing to do here.
  }

  /**
   * {@inheritdoc}
   */
  protected function loadFileMetadataArray(BackupFileInterface $file) {
    // Nothing to do here.
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($id) {
    $out = new BackupFile();
    $out->setMeta('id', $id);
    $filename = $this->base64url_decode($id);
    $out->setFullName($filename);
    return $out;
  }

  /**
   * Download backup file.
   *
   * @param \Drupal\backup_migrate\Core\File\BackupFileInterface $file
   *   Backup file.
   *
   * @return \Symfony\Component\HttpFoundation\Response|void
   *   Returns http file response.
   */
  public function downloadFile(string $id) {
    $api_key = $this->confGet('backbeaver_site_api_key');
    $this->backBeaverClient->setApiKey($api_key);

    $response = $this->backBeaverClient->downloadFile($id);
    if($response){
      $stream = $response->getBody();
      $content = new StreamedResponse(function () use ($stream) {
        /** @var StreamInterface $stream */
        while ($binary = $stream->read(1024)) {
          echo $binary;
          ob_flush();
          flush();
        }
      }, $response->getStatusCode(), [
        'Content-Type' => $response->getHeaderLine('Content-Type'),
        'Content-Length' => $response->getHeaderLine('Content-Length'),
        'Content-Disposition' => 'attachment; filename=' . $this->base64url_decode($id)
      ]);

      return $content;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function loadFileForReading(BackupFileInterface $file) {
// If this file is already readable, simply return it.
    if ($file instanceof BackupFileReadableInterface) {
      return $file;
    }

    $id = $file->getMeta('id');
    $api_key = $this->confGet('backbeaver_site_api_key');
    $this->backBeaverClient->setApiKey($api_key);

    $response = $this->backBeaverClient->downloadFile($id);

    if ($response) {
      $stream = $response->getBody();
      $filename = $this->base64url_decode($id);
      $file = $this->fileRepository->writeData($stream, "temporary://$filename", FileSystemInterface::EXISTS_REPLACE);
      if ($file) {
        return new ReadableStreamBackupFile($this->fileSystem->realpath($file->getFileUri()));
      }
    }

    $this->getLogger('backup_migrate_backbeaver')->error('Failed to download backup from BackBeaver.');
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function fileExists($id): bool {
    return (boolean) $this->getFile($id);
  }

  /**
   * {@inheritdoc}
   */
  public function listFiles($page=1, $count=100, $sortBy='created', $orderBy='DESC'): array {
    $api_key = $this->confGet('backbeaver_site_api_key');
    $this->backBeaverClient->setApiKey($api_key);
    $bb_files = $this->backBeaverClient->getFiles($page, $count, $sortBy, $orderBy);

    $files = [];
    if(isset($bb_files['error'])) {
      \Drupal::messenger()->addMessage($this->confGet('name').': '.$bb_files['error'], 'error');
    }
   else{
     foreach($bb_files as $bb_file){
       // Setup new backup file.
       $backupFile = new BackupFile();

       //Set ID as base64url of filename
       $id = $this->base64url_encode($bb_file['name']);
       $backupFile->setMeta('id', $id);
       $backupFile->setMeta('filesize', $bb_file['size']);
       $backupFile->setMeta('datestamp', strtotime($bb_file['created']));
       $backupFile->setFullName($bb_file['name']);
       $backupFile->setMeta('metadata_loaded', TRUE);

       // Add backup file to files array.
       $files[$id] = $backupFile;
     }
   }

    // Return files.
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function countFiles() {
    $file_list = $this->listFiles();
    return count($file_list);
  }

  /**
   * {@inheritdoc}
   */
  public function queryFiles(array $filters = [], $sort = 'datestamp', $sort_direction = SORT_DESC, $count = 100, $start = 0) {
    // Get the full list of files.
    if(is_null($count)) {
      //@todo: Check why Backup & Migrate uses count = NULL instead of pager?
      $count = 0;
      $page = 1;
    }
    else{
      $page = (int)(floor($start / $count) + 1);
    }

    //Match Backup & Migrate sort terminology with BackBeaver API sort keys
    $sortByMap = [
      'datestamp' => 'created',
      'filesize' => 'size',
      'name' => 'name',
    ];
    $sortBy = isset($sortByMap[$sort]) ? $sortByMap[$sort] : 'created';

    //@todo: investigate why Backup & Migrate inversed ASC/DESC order
    $orderByMap = [
      SORT_DESC => 'ASC',
      SORT_ASC => 'DESC'
    ];
    $orderBy = isset($orderByMap[$sort_direction]) ? $orderByMap[$sort_direction] : 'DESC';

    $out = $this->listFiles($page, $count, $sortBy, $orderBy);
    foreach ($out as $key => $file) {
      $out[$key] = $this->loadFileMetadata($file);
    }

    return $out;
  }


  /**
   * Init configurations.
   */
  public function configSchema($params = []): array {
    $schema = [];

    // Init settings.
    if ($params['operation'] == 'initialize') {
      //Adding markup elements doesn't work, so we will have to implement hook_form_alter and add markup there
      //Login/Register button or text
      /*$schema['fields']['help'] = [
        '#type' => 'item',
        '#title' => t('Need a Site API Key?'),
        '#markup' => t('Visit @BackBeaver.', array('@BackBeaver' => Link::fromTextAndUrl('BackBeaver', Url::fromUri("https://backbeaver.com/user")))),
      ];*/

      //Site API key
      $schema['fields']['backbeaver_site_api_key'] = [
        'type' => 'text',
        'title' => $this->t('Site API key'),
        'description' => $this->t('Enter your <a href=":url" target="_blank">BackBeaver</a> Site API key.' , array(':url' => "https://backbeaver.com")),
        'required' => TRUE,
      ];
    }

    return $schema;
  }

  /*
 * 'base64url' variant encoding
 * */
  private function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
  }

  /*
  * 'base64url' variant decode
   *  */
  private function base64url_decode($data) {
    return base64_decode(str_replace(array('-', '_'), array('+', '/'), $data));
  }
}
