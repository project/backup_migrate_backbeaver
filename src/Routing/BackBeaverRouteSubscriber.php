<?php

namespace Drupal\backup_migrate_backbeaver\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * BackBeaver Backup & Migrate Route Subscriber.
 */
class BackBeaverRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.backup_migrate_destination.backup_download')) {
      $route->setDefaults([
        '_controller' => '\Drupal\backup_migrate_backbeaver\Controller\BackBeaverBackupController::download',
      ]);
    }
  }
}
